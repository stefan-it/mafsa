/*
 * This file is part of mafsa.
 *
 * mafsa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file State.hpp
 *  @class State
 *  @brief Class definition for a state in the (minimal acyclic) automaton
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef STATE_HPP
#define STATE_HPP

#include <fstream>
#include <map>
#include <memory>

class State {
public:

  /** @brief Defines type for a state pointer
   */
  using StatePtr = std::shared_ptr<State>;

  /** @brief Defines type for a transition map. As key the label of a path
   * is used. As value the state where the label points to is used.
   */
  using TransitionMap = std::map<wchar_t, StatePtr>;

  State(size_t number) : is_final(false), number(number), transitions(){};

  /** @brief Checks, if two states are equivalent.
   *
   * Two states are equivalent when the following four conditions are met:
   *
   * 1. they are either both final or both nonfinal
   * 2. they have the same number of outgoing transitions
   * 3. corresponding outgoing transitions have same labels
   * 4. corresponding outgoing transitions lead to states that have the same
   *    states
   *
   * @param state Other state to be compared with
   * @return True, if the conditions above are met, False otherwise
   */
  auto is_equivalent(const StatePtr &other) -> bool const;

  /** @brief Defines if the current state has children
   *
   * @return True, if and only if there are outgoing transitions from the
   *               current state,
   *         False otherwise
   */
  auto has_children() -> bool const;

  /** @brief Finds the last state from current state
   *
   * This method returns a reference to the state reached by the
   * lexicographically last transition that is outgoing from the argument state.
   * Note: the input data is lexicographically sorted, so this function returns
   * the outgoing transition most recenctly added.
   *
   * Consider the following paths in the trie:
   *
   *                               (q3) ----> ...
   *                              /
   *                           a /
   *        a          b        /
   * (q0) ----> (q1) ----> (q2)
   *                            \
   *                           b \
   *                              \
   *                               (q4) ----> ...
   *
   * \------------------------/
   *              |
   *
   *        common prefix
   *
   * Word to be inserted is "abba". Then (q2) is the so called last state. The
   * path from the start state (q0) to the last sate (q2) is considered as the
   * common prefix of the word "abba". The path from last state to the end of
   * the to be inserted word is considered as current suffix.
   *
   * @return StatePtr Reference to the so called last state
   */
  auto last_child() -> StatePtr const;

  /** @brief Returns the state a given label leads to.
   *
   * This methods returns the state (in transition map) a given label leads to.
   *
   * @param label The given label
   * @return StatePtr Reference to the state a word leads to. If no label is
   *                  found in transition map, nullptr will be returned
   */
  auto delta(const wchar_t label) -> StatePtr const;

  /** @brief Writes dot representation of the state
   *
   * This method writes a dot representation of the complete state.
   *
   * @param out Output stream where the dot representation should be written to
   */
  auto write_dot(std::wostream &out, bool is_initial = false) -> void const;

  /** @brief Replaces the last child with a given state
   *
   * This method replaces the last child in transition map with a given state.
   *
   * @param state Replacing state for last child in transition map
   */
  auto replace_last_child(StatePtr state) -> void;

  /** @brief Adds transition for a given label to a given state
   *
   * This method adds a transition for a given label to a given state.
   *
   * @param label Label for new outgoing transition
   * @param state State for new outgoing transition
   */
  auto add_transition(const wchar_t label, StatePtr state) -> void;

  /** @brief Returns transition map for current state
   *
   * @return TransitionMap
   */
  auto get_transitions() -> auto;

  /** @brief Defines if the current state is final or not
   */
  bool is_final;

  /** @brief Defines the number for a state
   */
  size_t number;

protected:

  /** @brief Defines all outgoing transitions for a state
   */
  TransitionMap transitions;
};

auto State::is_equivalent(const StatePtr &other) -> bool const {
  return (this->is_final == other->is_final &&
          this->transitions == other->transitions);
}

auto State::delta(const wchar_t label) -> StatePtr const {
  return this->transitions.find(label) != this->transitions.end()
             ? this->transitions.at(label)
             : nullptr;
}

auto State::has_children() -> bool const { return !this->transitions.empty(); }

auto State::last_child() -> StatePtr const {
  return this->transitions.empty() ? nullptr
                                   : this->transitions.rbegin()->second;
}

auto State::write_dot(std::wostream &out, bool is_initial) -> void const {
  if (is_initial || this->is_final) {
    out << this->number << "[peripheries=" << (is_initial ? 3 : 2) << "];\n";
  }
  for (auto pair : this->transitions) {
    out << this->number << "->" << pair.second->number << "[label=\""
        << pair.first << "\"];\n";
  }
}

auto State::replace_last_child(StatePtr state) -> void {
  auto key = this->transitions.rbegin()->first;
  this->transitions[key] = state;
}

auto State::add_transition(const wchar_t label, StatePtr state) -> void {
  this->transitions[label] = state;
}

auto State::get_transitions() -> auto {
  return this->transitions;
}

#endif
