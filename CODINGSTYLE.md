# Coding style

In order to have a consistent code basis we are using llvm coding style.
More information can be found
[here](http://llvm.org/docs/CodingStandards.html). With the `clang-format`
tool, it is possible to auto-format source and header files in order to
pass the llvm coding style rules. Usage:

```bash
clang-format -style=llvm <file>
```

Output will be on stdout. It is also possible to use a kind of inplace
editing with the `-i` option:

```bash
clang-format -style=llvm -i <file>
```

More information on the clang-format tool can be found
[here](http://clang.llvm.org/docs/ClangFormat.html)

# Doxygen documentation

Every piece of source code (`*.c/cpp` and `*.h/hpp`) should
include doxygen comments in order to get a well documented project.
Please refer to the provided doxygen comment "pattern" below:

```cpp
/** @file file_name.hpp
 *  @class file_name
 *  @brief Short description
 *
 * Long description
 *
 * @author Latest <mail@addresse> (year)
 */
```

At least for the *latest* author, a **valid** mail address should be
stated (like: `Stefan Schweter <stefan@schweter.it>`) - here is a
*real world* header file:

```cpp
/** @file ExtractorFactory.hpp
 *  @class ExtractorFactory
 *  @brief Abstract factory for choosing an extractor method
 *
 * The ExtractorFactory class is an abstract factory implementation for choosing
 * an extractor.
 *
 * A derived class from this base class must define and overload the
 * get_extractor() method for choosing an extractor by its given name.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2016)
 */
```
