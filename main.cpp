#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <memory>

#include "MAFSA.hpp"
#include "State.hpp"

namespace po = boost::program_options;

static po::options_description DESC("Options");
static po::variables_map VARS;

static auto setup_description() -> void {
  DESC.add_options()("filename,f", po::value<std::string>(), "Input file")(
      "output-dot,o", po::value<std::string>()->default_value("automaton.dot"),
      "Output filename for dot file")

      ("help,h", "Displays help page");
}

static auto parse_program_options(int argc, char **argv) -> void {
  po::store(po::parse_command_line(argc, argv, DESC), VARS);
  po::notify(VARS);
}

auto main(int argc, char **argv) -> int {
  std::setlocale(LC_ALL, "");

  setup_description();
  parse_program_options(argc, argv);

  if (VARS.count("help")) {
    std::cout << DESC << std::endl;
    return EXIT_SUCCESS;
  }

  if (!VARS.count("filename")) {
    std::cout << "Please specify a filename to build an automaton!\n";
    std::cout << DESC << std::endl;
    return EXIT_SUCCESS;
  }

  std::wofstream output_fs(VARS["output-dot"].as<std::string>());
  output_fs.imbue(std::locale(""));

  auto mafsa = std::make_shared<MAFSA>();

  auto start_timer = std::clock();

  mafsa->construct_fsa(VARS["filename"].as<std::string>());

  auto duration = (std::clock() - start_timer) / (double)CLOCKS_PER_SEC;

  mafsa->write_dot(output_fs);

  output_fs.close();

  std::wcerr << "FSA construction time: " << duration << " seconds\n";

  mafsa->write_register(std::wcout);

  return 0;
}
