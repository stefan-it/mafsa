# "Incremental Construction of Minimal Acyclic Finite-State Automata" (MAFSA)

This is a C++14 header-only implementation of "Incremental Construction of
Minimal Acyclic Finite-State Automata" (MAFSA) which is described in [this](http://www.aclweb.org/anthology/J00-1002)
paper written by Jan Daciuk, Stoyan Mihov, Bruce Watson and Richard Watson.

**Notice**: This implementation is not *performance-optimized* as it uses the
C++11/C++14 *std::shared_ptr* interface.

## References

* "Incremental Construction of Minimal Acyclic Finite-State Automata" by
  Jan Daciuk, Stoyan Mihov, Bruce Watson and Richard Watson, see it [here](http://www.aclweb.org/anthology/J00-1002).

* This implementation was done during the computational linguistics M.Sc. course (winter term)
  ["Finite-State-Technologien beim Indizieren und Suchen"](http://www.cis.uni-muenchen.de/kurse/max/Master_P1_Basis/index.html)
  held by [Dr. Maximilian Hadersbeck](http://www.cis.lmu.de/personen/mitarbeiter/hadersbeck/index.html)
  and [Prof. Dr. Klaus U. Schulz](http://www.cis.uni-muenchen.de/people/schulz.html)
  at [The Center for Information and Language Processing (CIS)](http://www.cis.lmu.de/index.html), LMU Munich.

# Build status

[![build status](https://gitlab.com/stefan-it/mafsa/badges/master/build.svg)](https://gitlab.com/stefan-it/mafsa/commits/master)

# Compiling the example program

*MAFSA* will currently work on *Unix* and *Linux* based systems. There is no plan
to support Window$ in near future. Following dependencies must be installed to
build *MAFSA*:

* *GNU* `make`
* *GCC* >= 5.3 - as we heavily use new *C++11* and *C++14* features
* `clang` is fully supported - you are welcome to build *MAFSA* with that compiler
* `Boost` >= 1.44 - *MAFSA* needs: `program_options` and `unit_test_framework`
* `cmake` >= 2.8.8

After all dependencies have been installed, you can compile *MAFSA* with the
following commands:

```bash
mkdir build && cd $_
cmake ..
make
```

To execute the example program:

```bash
./bin/mafsa_example --help
Options:
  -f [ --filename ] arg                 Input file
  -o [ --output-dot ] arg (=automaton.dot)
                                        Output filename for dot file
  -h [ --help ]                         Displays help page

```

# Contact (Bugs, Feedback, Contribution and more)

For questions about *MAFSA*, contact the current maintainer:
Stefan Schweter <stefan@schweter.it>. If you want to contribute to the project
please refer to the [Contributing](CONTRIBUTING.md) guide!

# License

To respect the Free Software Movement and the enormous work of Dr. Richard Stallman
this implementation is released under the *GNU Affero General Public License*
in version 3. More information can be found [here](https://www.gnu.org/licenses/licenses.html)
and in `COPYING`.

# Acknowledgements

I want to thank [Dr. Maximilian Hadersbeck](http://www.cis.lmu.de/personen/mitarbeiter/hadersbeck/index.html)
and [Prof. Dr. Klaus U. Schulz](http://www.cis.uni-muenchen.de/people/schulz.html)
for their patience during the winter term course. Their great explanations
motivated and helped to deeply understand the algorithm!
