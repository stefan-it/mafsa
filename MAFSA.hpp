/*
 * This file is part of mafsa.
 *
 * mafsa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file MAFSA.hpp
 *  @class MAFSA
 *  @brief Incremental construction of a minimal acyclic finite-state automata
 *
 * This class constructs a minimal acyclic finite-state automate (incrementally)
 * based on the Daciuk, Mihov, Watson and Watson paper:
 *
 * http://www.aclweb.org/anthology/J00-1002
 *
 * Note: The data to be inserted needs to be sorted.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef MAFSA_HPP
#define MAFSA_HPP

#include <algorithm>
#include <fstream>
#include <memory>
#include <set>
#include <vector>

#include "State.hpp"

class MAFSA {
public:
  MAFSA() : start_state(nullptr), number_states(0), current_register(){};

  /** @brief Builds a (minimal) finite state automaton
   *
   * This methods builds an (minimal acyclic) finite state automaton. As input
   * a (sorted) std::set of words is used.
   *
   * @param words (Sorted) std::set of words
   */
  auto construct_fsa(const std::set<std::wstring> words) -> void;

  /** @brief Builds a (minimal) finite state automaton
   *
   * This methods builds an (minimal acyclic) finite state automaton. As input
   * a filename is used. The function recognizes one word per line in the input
   * file. The words need not to be sorted, as there are inserted into a (sorted)
   * std::set later.
   *
   * @param filename Filename of the to be read input file
   */
  auto construct_fsa(const std::string filename) -> void;

  /** @brief Checks, if a word exists in the automaton
   *
   * Internally this functions start a char-based search in the automaton. It
   * begins its search at the start state and follows possible transitions
   * over the path sequence (= word).
   *
   * The last state of the followed transitions must be a final state: the
   * word is then accepted by the automaton.
   *
   * @param word Word to be searched in the automaton
   * @return True if the word is accepted by the automaton,
   *         False otherwise
   */
  auto contains(const std::wstring word) -> bool const;

  /** @brief Writes dot representation of the automaton
   *
   * This method writes a dot representation of the complete automaton. It
   * iterates over all states stored in the register and calls the write_dot()
   * function for each state.
   *
   * @param out Output stream where the dot representation should be written to
   */
  auto write_dot(std::wostream &out) -> void const;

  /** @brief Outputs register of the automaton
   *
   * This method outputs the register of the automaton.
   *
   * @param out Output stream where the dot representation should be written to
   */
  auto write_register(std::wostream &out) -> void const;

protected:

  /** @brief Defines the register type
   */
  using Register = std::vector<State::StatePtr>;

  /** @brief Returns common prefix for a word
   *
   * This method finds the longest prefix of the word to be added that is a
   * prefix of a word already in the automaton.
   *
   * Consider the following paths in the trie:
   *
   *                               (q3) ----> ...
   *                              /
   *                           a /
   *        a          b        /
   * (q0) ----> (q1) ----> (q2)
   *                            \
   *                           b \
   *                              \
   *                               (q4) ----> ...
   *
   * \------------------------/
   *              |
   *
   *        common prefix
   *
   * Word to be inserted is "abba". Then (q2) is the so called last state. The
   * path from the start state (q0) to the last sate (q2) is considered as the
   * common prefix of the word "abba". The path from last state to the end of
   * the to be inserted word is considered as current suffix.
   *
   * @param word Word to be inserted into the automaton
   */
  auto common_prefix(const std::wstring word) -> std::wstring const;

  /** @brief Gets last state of a word
   *
   * This methods follows returns the last state for a word (starting at the
   * start state of the automaton).
   *
   * @param word Word to be traversed in the automaton
   */
  auto get_state(const std::wstring word) -> State::StatePtr const;

  /** @brief Replace equivalent states or adds state to register
   *
   * This function works on the last child of the passed state. The function
   * calls itself recursivly until it reaches the end of the path of the
   * previously added word. The depth of a word can be considered as the depth
   * of recursion. Returning from each recursive call, the function checks
   * whether a state equivalent to the passed state can be found in the register.
   * If true:   the state is replaced with the equivalent state found in the
   *            register.
   * Otherwise: the state is considered as a new representative of a class and
   *            is added to the register.
   *
   *  @param state (Last) state for a common prefix
   */
  auto replace_or_register(State::StatePtr state) -> void;

  /** @brief Adds new suffix starting from a state
   *
   * This function adds a new suffix (starting from a beginning state).
   *
   * @param state Beginning state for suffix
   * @param suffix Suffix to be inserted (start point is state)
   */
  auto add_suffix(State::StatePtr state, const std::wstring suffix) -> void;

  /** @brief Defines the start state
   */
  State::StatePtr start_state;

  /** @brief Defines number of states in the automaton
   */
  size_t number_states;

  /** @brief Defines the register for the automaton
   *
   * The register contains only states that are pairwise inequivalent (incl.
   * the start state).
   */
  Register current_register;
};

auto MAFSA::common_prefix(const std::wstring word) -> std::wstring const {
  std::wstring prefix{L""};

  State::StatePtr current_state = this->start_state;

  for (auto c : word) {
    current_state = current_state->delta(c);
    if (!current_state) {
      break;
    } else {
      prefix += c;
    }
  }
  return prefix;
}

auto MAFSA::get_state(const std::wstring word) -> State::StatePtr const {
  State::StatePtr current_state = this->start_state;
  for (auto c : word) {
    current_state = current_state->delta(c);
    if (!current_state) {
      return nullptr;
    }
  }
  return current_state;
}

auto MAFSA::add_suffix(State::StatePtr state, const std::wstring suffix) -> void {
  State::StatePtr current_state = state;

  for (auto c : suffix) {
    auto new_child = std::make_shared<State>(this->number_states++);
    current_state->add_transition(c, new_child);
    current_state = new_child;
  }
  current_state->is_final = true;
}

auto MAFSA::replace_or_register(State::StatePtr state) -> void {
  State::StatePtr child = state->last_child();

  if (child->has_children()) {
    this->replace_or_register(child);
  }

  auto equivalent_checker_it = std::find_if(
      this->current_register.cbegin(), this->current_register.cend(),
      [&child](auto &other) { return child->is_equivalent(other); });

  if (equivalent_checker_it != this->current_register.cend()) {
    state->replace_last_child(*equivalent_checker_it);
  } else {
    this->current_register.emplace_back(child);
  }
}

auto MAFSA::contains(const std::wstring word) -> bool const {
  auto state = this->get_state(word);
  return state && state->is_final;
}

auto MAFSA::write_dot(std::wostream &out) -> void const {
  out << "Digraph MDA {\n"
      << "rankdir=LR;\n"
      << "ordering=out;\n";
  this->start_state->write_dot(out, true);

  for (auto state : this->current_register) {
    state->write_dot(out);
  }

  out << "s[height=0.1, width=0.1, fixedsize = true, fontsize = 0.1, "
         "fontcolor=blue, style=filled, fillcolor=blue, color=blue];\n";
  out << "edge[color=blue];\ns->0;";

  out << "}\n";
}

auto MAFSA::construct_fsa(const std::set<std::wstring> words) -> void {
  this->start_state = std::make_shared<State>(this->number_states++);

  for (auto word : words) {
    auto common_prefix = this->common_prefix(word);

    auto last_state = this->get_state(common_prefix);

    auto current_suffix = word.substr(common_prefix.size(), word.size());

    if (last_state->has_children()) {
      this->replace_or_register(last_state);
    }

    this->add_suffix(last_state, current_suffix);
  }
  this->replace_or_register(this->start_state);
}

auto MAFSA::construct_fsa(const std::string filename) -> void {
  std::wifstream input_fs(filename.c_str());
  input_fs.imbue(std::locale(""));

  std::set<std::wstring> words;
  std::wstring word;

  while (std::getline(input_fs, word)) {
    words.emplace(word);
  }

  this->construct_fsa(words);
}

auto MAFSA::write_register(std::wostream &out) -> void const {
  for (auto s : this->current_register) {
    out << s->number << "\n";
  }
}

#endif
